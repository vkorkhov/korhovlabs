#include "/usr/include/mpich/mpi.h"
#include <stdio.h>
#include <math.h>

static void initArray(int *array, int arraySize, int maxInitValue)
{
  // Fill array
  for (int i = 0; i < arraySize; i++)
  {
    array[i] = i % maxInitValue;
  }
  printf("Filled\n");
}

int main(int argc, char *argv[])
{
  printf("1\n");

  int currentId, procCount, nameLenght;
  double startwtime = 0.0, endwtime;
  char procName[MPI_MAX_PROCESSOR_NAME];
  int arraySize = 200000;
  int maxInitValue = 1000;
  int *vectorFirst;
  int *vectorSecond;

  printf("2\n");

  int minValue;
  int maxValue;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &procCount);
  MPI_Comm_rank(MPI_COMM_WORLD, &currentId);
  MPI_Get_processor_name(procName, &nameLenght);

   printf("3\n");

  fprintf(stderr, "Process %d on %s\n", currentId, procName);

  vectorFirst = new int[arraySize];
  vectorSecond = new int[arraySize];
  printf("Memory\n");

  initArray(vectorFirst, arraySize, maxInitValue);
  initArray(vectorSecond, arraySize, maxInitValue);

  printf("Inited\n");

  if (currentId == 0)
  {
    double s_time = MPI_Wtime();
    int pSum = 0;
    for (int i = 0; i < arraySize; ++i)
    {
      pSum += (vectorFirst[i] * vectorSecond[i]);
    }
    double e_time = MPI_Wtime();
    printf("Target sum = %d, time ellapsed: %f\n", pSum, e_time - s_time);
  }

  double sp_time = MPI_Wtime();

  printf("Before bcast\n");

  MPI_Bcast(vectorFirst, arraySize, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(vectorSecond, arraySize, MPI_INT, 0, MPI_COMM_WORLD);

  printf("After bcast\n");

  // Get calculation edges
  int chunkSize = arraySize / procCount;
  int from = chunkSize * currentId;
  int to = chunkSize * (currentId + 1);
  if (currentId == procCount - 1)
  {
    to = arraySize;
  }

  // Sum var
  int sum = 0;
  int currentSum = 0;

  for (int i = from; i < to; ++i)
  {
    currentSum += vectorFirst[i] * vectorSecond[i];
  }

  MPI_Reduce(&currentSum, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  if (currentId == 0)
  {
    double ep_time = MPI_Wtime();
    printf("Sum = %d, time ellapsed: %f\n", sum, ep_time - sp_time);
  }

  MPI_Finalize();

  printf("Success\n");

  return 0;
}
