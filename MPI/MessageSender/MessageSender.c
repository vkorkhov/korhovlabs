#include "/usr/include/mpich/mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Recieve message from process 0/1 and send to process 1/0
void ResendMessage(int recProcId, void *buf)
{
  MPI_Status Status;

  int senderProcId = 0;
  if (recProcId == 0)
  {
    senderProcId = 1;
  }

  MPI_Recv(buf, 1, MPI_BYTE, senderProcId, MPI_ANY_TAG, MPI_COMM_WORLD, &Status);
  printf("Message recieved from process %d\n", senderProcId);
  printf("Resending message to process %d...\n", senderProcId);
  MPI_Send(buf, 1, MPI_BYTE, senderProcId, 0, MPI_COMM_WORLD);
}

void createMessage(void *message, int messageSize)
{
  free(message);
  message = malloc(messageSize * 8);
}

int main(int argc, char *argv[])
{
  // MPI default variables
  int procId, procCount, nameLenght;
  char procName[MPI_MAX_PROCESSOR_NAME];

  // Message to be sended
  void *message;

  // Messages sizes in bytes
  int const messageSizesCount = 8;
  int messageSizes[8] = {8, 16, 32, 64, 128, 256, 512, 1024};

  // Message count which should be sended by each process
  int messageCount = 10;

  // MPI initialization
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &procCount);
  MPI_Comm_rank(MPI_COMM_WORLD, &procId);
  MPI_Get_processor_name(procName, &nameLenght);
  fprintf(stderr, "Process %d on %s\n", procId, procName);

  // For current lab process count shoulb be not less than 2
  if (procCount < 2)
  {
    if (procId == 0)
    {
      fprintf(stderr, "Process count should be not less than 2, current count = %d\n", procCount);
    }
    //return 0;
  }

  double s_time;
  double e_time;

  // For each message size value
  for (int i = 0; i < messageSizesCount; ++i)
  {
    createMessage(message, messageSizes[i]);

    if (procId == 0)
    {
      s_time = MPI_Wtime();
      
      // First message should be sended from master process
      MPI_Send(message, 1, MPI_BYTE, 1, 0, MPI_COMM_WORLD);
    }

    int currentMessageCount = 0;
    while (currentMessageCount < messageCount)
    {
      ResendMessage(procId, message);
      ++currentMessageCount;
    }

    if (procId == 0)
    {
      e_time = MPI_Wtime();
      printf("Ellapsed time for message with %d bytes size is %f \n", messageSizes[i], e_time-s_time);
    }
  }

  MPI_Finalize();

  return 0;
}
