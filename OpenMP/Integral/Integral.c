#include <omp.h>
#include <stdio.h>

float f(float x)
{
  return x*x + x + 5;
}


int main()
{
  // Init params
  int N = 1000;
  float a = 0;
  float b = 10;
  float h = (b - a) / N;

  // Init sum
  float sum = 0;

#pragma omp parallel shared(N, a, b, h)
  {
#pragma omp for schedule(guided) reduction(+: sum)
    for (int i = 0; i < N; ++i)
    {
      float x = i*h;
      sum += h * f(x);
    }
  }

  printf("Integral value = %f\n", sum);
}