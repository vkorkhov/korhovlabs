#include <omp.h>
#include <stdio.h>

int main()
{
  int vectorSize = 1000000;
  int maxInitValue = 1000;

  printf("Vector size = %d\n", vectorSize);

  // Fill vectors
  int vectorFirst[vectorSize];
  int vectorSecond[vectorSize];
  for (int i = 0; i < vectorSize; i++)
  {
    vectorFirst[i] = i % maxInitValue;
    vectorSecond[i] = i % maxInitValue;
  }

  // Get chunk size
  int threadCount = omp_get_max_threads();
  int chunkSize = vectorSize / threadCount;

  // Init sum
  int sum = 0;

  double startTime = omp_get_wtime();

  for (int i = 0; i < vectorSize; ++i)
  {
    sum += vectorFirst[i] * vectorSecond[i];
  }

  double endTime = omp_get_wtime();
  double timeSpentNotparallel = (endTime - startTime);

  printf("Time spent not parallel  = %f\n", timeSpentNotparallel);

  sum = 0;

  startTime = omp_get_wtime();

#pragma omp parallel shared(vectorFirst, vectorSecond, vectorSize)
  {
#pragma omp for schedule(dynamic, chunkSize) reduction(+: sum)
    for (int i = 0; i < vectorSize; ++i)
    {
      sum += vectorFirst[i] * vectorSecond[i];
    }
  }

  endTime = omp_get_wtime();
  double timeSpentParallel = (endTime - startTime);

  printf("Time spent in parallel = %f\n", timeSpentParallel);

  double acc = timeSpentNotparallel/timeSpentParallel;

  printf("Acceleration = %f\n", acc);
}