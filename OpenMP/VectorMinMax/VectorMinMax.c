#include <omp.h>
#include <stdio.h>

int main()
{
  int arraySize = 1600000;
  int maxInitValue = 90;

  printf("Vector size = %d\n", arraySize);

  // Fill array
  int array[arraySize];
  for (int i = 0; i < arraySize; i++)
  {
    array[i] = i % maxInitValue;
  }

  // Get chunk size
  int threadCount = omp_get_max_threads();
  int chunkSize = arraySize / threadCount;

  // Init min and max vules
  int minValue = maxInitValue;
  int maxValue = 0;

double startTime = omp_get_wtime();

#pragma omp parallel shared(array, arraySize, chunkSize, minValue, maxValue)
  {
#pragma omp for schedule(dynamic, chunkSize)
    for (int i = 0; i < arraySize; ++i)
    {
      // Check max value
      if (array[i] > maxValue)
      {
#pragma omp critical
        if (array[i] > maxValue)
        {
          maxValue = array[i];
        }
      }

      // Check min value
      if (array[i] < minValue)
      {
#pragma omp critical
        if (array[i] < minValue)
        {
          minValue = array[i];
        }
      }
    }
  }

  double endTime = omp_get_wtime();
  double timeSpent = (endTime - startTime);

  printf("Time spent in parallel = %f\n", timeSpent);

  startTime = omp_get_wtime();

  for (int i = 0; i < arraySize; ++i)
    {
      // Check max value
      if (array[i] > maxValue)
      {
        if (array[i] > maxValue)
        {
          maxValue = array[i];
        }
      }

      // Check min value
      if (array[i] < minValue)
      {
        if (array[i] < minValue)
        {
          minValue = array[i];
        }
      }
    }
      
  endTime = omp_get_wtime();
  timeSpent = (endTime - startTime);

  printf("Time spent non-parallel = %f\n", timeSpent);
}